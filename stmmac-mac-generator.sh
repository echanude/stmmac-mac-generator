#!/usr/bin/env bash
# SPDX-License-Identifier: GPL-2.0

# Grab the board serial number given by androidboot.serialno in /proc/cmdline
# and use that to generate a MAC address that will be stable for this
# particular system.

set -e

OUI="8a:12:4e" # The OUI is Qualcomm, with the U/L bit set to local

__SN="$(</proc/cmdline)"
_SN="${__SN##*androidboot.serialno=}"

NEW_MAC_23000000=""
NEW_MAC_20000=""

if [ -z "$_SN" ]
then # Use machine-id as fallback if serial number is not present
    MI=$(cat /etc/machine-id)
    MI_6=${MI:0:6}
    MI_6_1="$(printf "%06x" "$(((16#${MI_6}+1) % 16#1000000))")" # For second NIC, add one

    NEW_MAC_23000000="${OUI}:${MI_6:0:2}:${MI_6:2:2}:${MI_6:4:2}"
    NEW_MAC_20000="${OUI}:${MI_6_1:0:2}:${MI_6_1:2:2}:${MI_6_1:4:2}"
else
    SN="${_SN%% *}"
    SN_6=${SN:0:6}
    SN_6_1="$(printf "%06x" "$(((16#${SN_6}+1) % 16#1000000))")" # For second NIC, add one

    NEW_MAC_23000000="${OUI}:${SN_6:0:2}:${SN_6:2:2}:${SN_6:4:2}"
    NEW_MAC_20000="${OUI}:${SN_6_1:0:2}:${SN_6_1:2:2}:${SN_6_1:4:2}"
fi

mkdir -p /run/systemd/network/
cat > /run/systemd/network/10-stmmac-1gb-23000000.link << __EOF__
[Match]
Path=platform-2300000*

[Link]
MACAddress=${NEW_MAC_23000000}
__EOF__

echo "Created 10-stmmac-1gb-23000000.link file with MAC address ${NEW_MAC_23000000}"

cat > /run/systemd/network/10-stmmac-1gb-20000.link << __EOF__
[Match]
Path=platform-20000*

[Link]
MACAddress=${NEW_MAC_20000}
__EOF__

echo "Created 10-stmmac-1gb-20000.link file with MAC address ${NEW_MAC_20000}"
