The 1GB stmmac NICs on the Qualcomm SA8540p Development Boards (QDrive3) come up with
a random hardware MAC address.

This RPM package uses systemd to rewrite the random MAC address into one that's based
on the board serial number. A systemd link file
`/run/systemd/network/10-stmmac-1gb-<NIC address>.link` is created for each NIC with
contents similar to the following:

    [Match]
    Path=platform-2300000*
        
    [Link]
    MACAddress=8a:12:4e:50:72:49

systemd expects this file to be static, and does not have the ability to dynamically
inject a new MAC address. This RPM provides a script that'll dynamically create a MAC
address within the Qualcomm OUI. This script is automatically executed as a post
install scriptlet after the RPM is installed. It's also ran during bootup before the
networking is started. This will ensure that each board keeps the same MAC address
that's unique across the fleet.

Note that this RPM is just a workaround until we find a solution from Qualcomm about
how to program a MAC address into our boards.

# Credits

This package is based on the work Brian Masney did to fix a similar issue with the
10GB Aquantia interface : https://github.com/masneyb/aquantia-mac-generator
